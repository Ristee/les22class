<?php

/**
 * Created by PhpStorm.
 * User: ristee
 * Date: 25/11/15
 * Time: 22:27
 */
class NewsController
{
    public function actionAll()
    {
        $items = News::getAll();
        include __DIR__ . '/../views/news/all.php';
    }

    public function actionOne()
    {
        $id = $_GET['id'];
        $item = News::getOne($id);
        include __DIR__ . '/../views/news/one.php';
    }
}