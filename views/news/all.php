<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<?php foreach ($items as $item) : ?>
    <h1><?= $item->title ?></h1>
    <p><?= $item->text ?></p>
    <hr>
<?php endforeach ?>
</body>
</html>