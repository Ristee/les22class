<?php

/**
 * Created by PhpStorm.
 * User: ristee
 * Date: 25/11/15
 * Time: 21:40
 */
class News extends AbstractModel
{
    public $id;
    public $title;
    public $text;

    protected static $table = 'news';
    protected static $class = 'News';


}