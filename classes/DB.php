<?php

/**
 * Created by PhpStorm.
 * User: ristee
 * Date: 25/11/15
 * Time: 21:22
 */
class DB
{
    private $mysql;

    public function __construct()
    {
        $this->mysql = new mysqli("localhost", "root", "", "mydatabase");
    }

    public function queryAll($sql, $class = 'stdClass')
    {
        $res = $this->mysql->query($sql);
        if ($res === false) {
            return false;
        }
        $ret = [];
        while ($row = $res->fetch_object($class)) {
            $ret[] = $row;
        }
        return $ret;
    }

    public function queryOne($sql, $class = 'stdClass')
    {
        return $this->queryAll($sql, $class)[0];
    }
}