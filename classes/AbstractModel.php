<?php

/**
 * Created by PhpStorm.
 * User: ristee
 * Date: 26/11/15
 * Time: 00:53
 */
abstract class AbstractModel
{
    protected static $table;
    protected static $class;

    public static function getAll()
    {
        $db = new DB;
        $sql = 'select * from ' . static::$table;
        return $db->queryAll($sql, static::$class);
    }

    public static function getOne($id)
    {
        $db = new DB();
        $sql = 'select * from ' . static::$table . ' where id=$id';
        return $db->queryOne($sql, static::$class);
    }
}